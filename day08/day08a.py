from parse import parse

with open("day08/input") as f:
    ds, ns = f.read().split("\n\n")
ns = ns.splitlines()
ns = [parse("{} = ({}, {})", node) for node in ns]

t = dict((a, (b, c)) for a, b, c in ns)

s = 0
l = len(ds)
c = "AAA"
while c != "ZZZ":
    c = t[c][0] if ds[s % l] == "L" else t[c][1]
    s += 1

print(s)
