from parse import parse
from math import lcm

with open("day08/input") as f:
    ds, ns = f.read().split("\n\n")
ns = ns.splitlines()
ns = [parse("{} = ({}, {})", n) for n in ns]
t = dict((a, (b, c)) for a, b, c in ns)

l = len(ds)

a = 1
for c in t:
    if c.endswith("A"):
        s = 0
        while not c.endswith("Z"):
            c = t[c][0] if ds[s % l] == "L" else t[c][1]
            s += 1
        a = lcm(a, s)
print(a)
