from collections import Counter

with open("day07/input") as f:
    ls = f.read().splitlines()

def type(cards):
    c = sorted(Counter(cards).values())
    if c[-1] == 5: return 7
    if c[-1] == 4: return 6
    if c[-1] == 3:
        if c[-2] == 2: return 5
        return 4
    if c[-1] == 2:
        if c[-2] == 2: return 3
        return 2
    return 1

rank = "23456789TJQKA"
a = [l.split() for l in ls]
b = sorted([(type(cards), [rank.index(c) for c in cards], bid) for cards, bid in a])
print(sum(i * int(bid) for i, (_, _, bid) in enumerate(b,1)))
