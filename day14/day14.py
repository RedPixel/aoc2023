def load(ls):
    return sum(l.count("O") * i for i, l in enumerate(ls[::-1], 1))

def transpose(ls):
    return ["".join(l) for l in zip(*ls)]

def west(ls):
    return ["#".join(("".join(sorted(p, reverse=True)) for p in line.split("#"))) for line in ls]

def north(ls):
    return transpose(west(transpose(ls)))

def east(ls):
    return ["#".join(("".join(sorted(p)) for p in line.split("#"))) for line in ls]

def south(ls):
    return transpose(east(transpose(ls)))

def cycle(ls):
    return east(south(west(north(ls))))

def repeat(ls, times):
    seen = {}
    state = tuple(ls)
    i = 0
    while state not in seen:
        seen[state] = i
        ls = cycle(ls)
        state = tuple(ls)
        i += 1
    period = i - seen[state]
    rest = (times - i) % period
    for _ in range(rest):
        ls = cycle(ls)
    return ls

with open("day14/input") as f:
    ls = f.read().splitlines()
print(load(north(ls)))
print(load(repeat(ls, 1000000000)))
