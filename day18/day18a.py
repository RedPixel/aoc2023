def volume(plan):
    curr = 0
    meters = 1
    for (x, y), n in plan:
        curr += x * n
        meters += y * n * curr + (n / 2)
    return meters


with open("day18/input") as f:
    ls = f.read().splitlines()

dirs = {"D": (0, 1), "U": (0, -1), "L": (-1, 0), "R": (1, 0)}
plan = [(dirs[d], int(n)) for d, n, _ in (l.split() for l in ls)]
print(int(volume(plan)))
