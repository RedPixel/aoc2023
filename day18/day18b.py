def volume(plan):
    curr = 0
    meters = 1
    for (x, y), n in plan:
        curr += x * n
        meters += y * n * curr + (n / 2)
    return meters


with open("day18/input") as f:
    ls = f.read().splitlines()

dirs = {"0": (1, 0), "1": (0, 1), "2": (-1, 0), "3": (0, -1)}
plan = [(dirs[c[7]], int(c[2:7], 16)) for _, _, c in (l.split() for l in ls)]
print(int(volume(plan)))
