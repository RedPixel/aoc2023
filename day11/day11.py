def dist(cs, rows):
    ds = [sum(1 if p in cs else rows for p in range(c)) for c in cs]
    return sum(abs(a - b) for a in ds for b in ds) // 2


with open("day11/input") as f:
    ls = f.read().splitlines()


cs = [(x, y) for y, r in enumerate(ls) for x, c in enumerate(r) if c == "#"]
xs = [c[0] for c in cs]
ys = [c[1] for c in cs]
print(dist(xs, 2) + dist(ys, 2))
print(dist(xs, 1000000) + dist(ys, 1000000))
