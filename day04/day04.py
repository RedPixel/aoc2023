with open("day04/input") as f:
    ls = f.read().splitlines()

pts = 0
cards = [1] * len(ls)

for i, l in enumerate(ls):
    n = 35 - len({int(s) for s in l.split(": ")[1].split() if s.isdigit()})
    if n > 0:
        pts += 2 ** (n - 1)
    for j in range(n):
        cards[i + j + 1] += cards[i]
print(pts, sum(cards))
