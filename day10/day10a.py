def get_start(ls):
    for y, l in enumerate(ls):
        for x, c in enumerate(l):
            if c == "S":
                return (x, y)


def count_steps(c, x, y, dx, dy):
    steps = 0
    while True:
        x += dx
        y += dy
        c = ls[y][x]
        steps += 1
        match c:
            case "L":
                dx, dy = (1, 0) if dx == 0 else (0, -1)
            case "J":
                dx, dy = (-1, 0) if dx == 0 else (0, -1)
            case "7":
                dx, dy = (-1, 0) if dx == 0 else (0, 1)
            case "F":
                dx, dy = (1, 0) if dx == 0 else (0, 1)
            case "S":
                return steps


def find_loop(ls, sx, sy):
    dirs = [(0, 1), (0, -1), (1, 0), (-1, 0)]
    for dx, dy in dirs:
        print(count_steps(ls, sx, sy, dx, dy) // 2)  # the one printed twice is correct


with open("day10/input") as f:
    ls = f.read().splitlines()

sx, sy = get_start(ls)
find_loop(ls, sx, sy)
