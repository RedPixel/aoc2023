def get_start(ls):
    for y, l in enumerate(ls):
        for x, c in enumerate(l):
            if c == "S":
                return (x, y)


def get_border(c, x, y, dx, dy):
    border = set()
    while True:
        x += dx
        y += dy
        border.add((x, y))
        c = ls[y][x]
        match c:
            case "L":
                dx, dy = (1, 0) if dx == 0 else (0, -1)
            case "J":
                dx, dy = (-1, 0) if dx == 0 else (0, -1)
            case "7":
                dx, dy = (-1, 0) if dx == 0 else (0, 1)
            case "F":
                dx, dy = (1, 0) if dx == 0 else (0, 1)
            case "S":
                return border


with open("day10/input") as f:
    ls = f.read().splitlines()

sx, sy = get_start(ls)
border = get_border(ls, sx, sy, 1, 0)
count = 0
for y in range(len(ls)):
    inside = False
    for x in range(len(ls[0])):
        if (x, y) in border:
            if ls[y][x] in "|JL":
                inside = not inside
        else:
            if inside:
                count += 1
print(count)
