from collections import defaultdict


def hash(l):
    h = 0
    for c in l:
        h = ((h + ord(c)) * 17) % 256
    return h


with open("day15/input") as f:
    ls = f.read().split(",")

print(sum(hash(l) for l in ls))

bs = defaultdict(dict)
for l in ls:
    if l[-1] == "-":
        label = l[:-1]
        h = hash(label)
        if label in bs[h]:
            bs[h].pop(label)
    else:
        label, n = l.split("=")
        h = hash(label)
        bs[h][label] = int(n)

print(sum((b + 1) * i * v for b in bs for i, v in enumerate(bs[b].values(), 1)))
