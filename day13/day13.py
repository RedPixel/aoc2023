def count(p, s):
    for i in range(len(p)):
        a = p[i:]
        b = p[i-1::-1]
        if sum(c != d for l, m in zip(a, b) for c, d in zip(l, m)) == s:
            return i
    return 0

def transpose(p):
    return [[row[i] for row in p] for i in range(len(p[0]))]


with open("day13/input") as f:
    ps = [l.split() for l in f.read().split("\n\n")]
print(sum(100 * count(p, 0) + count(transpose(p), 0) for p in ps))
print(sum(100 * count(p, 1) + count(transpose(p), 1) for p in ps))
