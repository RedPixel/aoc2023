from collections import defaultdict
from copy import deepcopy
from math import prod
from parse import parse


def parse_input(workflows):
    ws = defaultdict(list)
    for w in workflows:
        name, rules = parse("{}{{{}}}", w)
        ws[name].extend(parse_rule(rule) for rule in rules.split(','))
    return ws

def parse_rule(rule):
    if ':' in rule:
        var, op, val, next = parse('{:l}{:W}{:d}:{:w}', rule)
        return (var, op, int(val), next)
    else:
        return rule


def splits(curr, rules, rs):
    if curr == "A":
        return prod(rs[key][1] - rs[key][0] + 1 for key in "xmas")
    if curr == "R":
        return 0

    rule = rules[curr].pop(0)
    if type(rule) is tuple:
        var, op, val, next = rule
        r_true = deepcopy(rs)
        r_false = deepcopy(rs)
        if op == ">":
            r_true[var][0] = val + 1
            r_false[var][1] = val
        else:
            r_true[var][1] = val - 1
            r_false[var][0] = val
        return splits(next, rules, r_true) + splits(curr, rules, r_false)
    else:
        return splits(rule, rules, rs)


with open("day19/input") as f:
    workflows, _ = map(str.splitlines, f.read().split("\n\n"))
ws = parse_input(workflows)
ranges = {"x": [1, 4000], "m": [1, 4000], "a": [1, 4000], "s": [1, 4000]}
print(splits("in", ws, ranges))
