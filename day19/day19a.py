from collections import defaultdict
from parse import parse


def parse_input(workflows):
    ws = defaultdict(list)
    for w in workflows:
        name, rules = parse("{}{{{}}}", w)
        ws[name].extend(parse_rule(rule) for rule in rules.split(','))
    return ws

def parse_rule(rule):
    if ':' in rule:
        var, op, val, next = parse('{:l}{:W}{:d}:{:w}', rule)
        return (var, op, int(val), next)
    else:
        return rule


def rate(rules, vals):
    curr = "in"
    while curr != "A" and curr != "R":
        for rule in rules[curr]:
            if type(rule) is tuple:
                var, op, val, next = rule
                if op == "<" and vals[var] < val:
                    curr = next
                    break
                elif op == ">" and vals[var] > val:
                    curr = next
                    break
            else:
                curr = rule
    if curr == "A":
        return True
    if curr == "R":
        return False


with open("day19/input") as f:
    workflows, ratings = map(str.splitlines, f.read().split("\n\n"))
ws = parse_input(workflows)

valss = []
for rating in ratings:
    x, m, a, s = parse("{x={:d},m={:d},a={:d},s={:d}}", rating)
    valss.append({"x": x, "m": m, "a": a, "s": s})

print(sum(sum(vals.values()) if rate(ws, vals) else 0 for vals in valss))
