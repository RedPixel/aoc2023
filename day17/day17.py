from queue import PriorityQueue

with open("day17/input") as f:
    ls = f.read().splitlines()
h = len(ls)
w = len(ls[0])


def short(min_len, max_len):
    q = PriorityQueue()
    q.put((0, 0, 0, 0, 0))
    seen = set()
    while q:
        heat, x, y, dx, dy = q.get()
        if x == w - 1 and y == h - 1:
            return heat
        if (x, y, dx, dy) in seen:
            continue
        seen.add((x, y, dx, dy))
        for ndx, ndy in [(1, 0), (-1, 0), (0, 1), (0, -1)]:
            if (ndx, ndy) != (dx, dy) and (ndx, ndy) != (-dx, -dy):
                nheat, nx, ny = heat, x, y
                for d in range(1, max_len + 1):
                    nx, ny = nx + ndx, ny + ndy
                    if 0 <= nx < w and 0 <= ny < h:
                        nheat += int(ls[ny][nx])
                        if d >= min_len:
                            q.put((nheat, nx, ny, ndx, ndy))


print(short(1, 3))
print(short(4, 10))
