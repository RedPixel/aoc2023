with open("day16/input") as f:
    ls = f.read().splitlines()
h = len(ls)
w = len(ls[0])

def beam(q):
    seen = set()
    while q:
        x, y, dx, dy = q.pop()
        x, y = x + dx, y + dy
        if (x, y, dx, dy) not in seen and 0 <= x < w and 0 <= y < h:
            seen.add((x, y, dx, dy))
            match ls[y][x]:
                case "|": q += [(x, y, 0, -1), (x, y, 0, 1)] if dx != 0 else [(x, y, dx, dy)]
                case "-": q += [(x, y, -1, 0), (x, y, 1, 0)] if dy != 0 else [(x, y, dx, dy)]
                case "/": q += [(x, y, -dy, -dx)]
                case "\\": q += [(x, y, dy, dx)]
                case ".": q += [(x, y, dx, dy)]
    return len(set((x, y) for (x, y, _, _) in seen))

print(beam([(-1, 0, 1, 0)]))
print(max(max(beam([(i, -1, 0, 1)]), beam([(i, h, 0, -1)]), beam([(-1, i, 1, 0)]), beam([(w, i, -1, 0)])) for i in range(w)))
