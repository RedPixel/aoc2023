ts = [48, 87, 69, 81]
ds = [255, 1288, 1117, 1623]

total = 1
for t, d in zip(ts, ds):
    total *= sum(1 for x in range(t + 1) if x * (t - x) >= d)

print(total)