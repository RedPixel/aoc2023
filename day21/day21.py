grid = {}
with open("day21/input") as f:
    for x, r in enumerate(f):
        for y, c in enumerate(r):
            if c == ".":
                grid[(x, y)] = c
            if c == "S":
                grid[(x, y)] = c
                q = {(x, y)}
a = []

dirs = [(1, 0), (-1, 0), (0, 1), (0, -1)]
for s in range(3 * 131):
    if s == 64:
        print(len(q))
    if s % 131 == 65:
        a.append(len(q))

    q = { 
        (p[0] + d[0], p[1] + d[1]) for d in dirs for p in q if (
                ((p[0] + d[0] % 131) + 131) % 131,
                ((p[1] + d[1] % 131) + 131) % 131,
        ) in grid
    }

n = 26501365 // 131
print(a[0] + n * (a[1] - a[0] + (n - 1) * (a[2] - 2 * a[1] + a[0]) // 2))
