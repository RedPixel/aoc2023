with open("day09/input") as f:
    ns = [[int(i) for i in l.split()] for l in f.read().splitlines()]

def f(ns):
    return ns[-1] + f([ns[i+1]-ns[i] for i in range(len(ns)-1)]) if any(ns) else 0

print(sum(f(n) for n in ns))
print(sum(f(n[::-1]) for n in ns))
